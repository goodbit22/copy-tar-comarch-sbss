#!/usr/bin/env python3
import docker
import tarfile
import json
from os import remove
from argparse import ArgumentParser
from docker.errors import APIError
from docker import from_env
from scp import SCPClient
from tarfile import ReadError, is_tarfile
from os.path import exists, basename, isfile
from json import load
from sshConnection import createSSHClient


ehc_environments = {"ehc_dev-crm1": ""}

valo_environments = {"valo_dev-crm1": "o"}

cmp_environments = {"cmp_dev-crm1": "o"}


def find_server_env(deploy_environmets, env, server):
    data = {}
    for keys in deploy_environmets:
        if keys.find(f"_{env}") != -1:
            if keys.find(server) != -1:
                print(keys, "->", deploy_environmets[keys])
                data[keys] = deploy_environmets[keys]
    return data


def find_deploy(deploy, env, server):
    if deploy == "ehc":
        data = find_server_env(ehc_environments, env, server)
    elif deploy == "valo":
        data = find_server_env(valo_environments, env, server)
    elif deploy == "cmp":
        data = find_server_env(cmp_environments, env, server)
    return data


def check_json(json_filename="manifest.json"):
    f = open(json_filename)
    config = []
    content = json.load(f)
    keys_manifest = ["Config", "RepoTags", "Layers"]
    f.close()
    keys_f = list(dict(content[0]).keys())
    if keys_f == keys_manifest:
        print("klucze sie zgadza")
        for i in content:
            for ke in keys_manifest:
                if ke == "Config":
                    config.append(i[ke])
                elif ke == "Layers":
                    layers = i[ke]
        remove(json_filename)
        return config, layers


def file_exist(path_file):
    if (
        exists(path_file)
        and isfile(path_file)
        and str(basename(path_file)).endswith(".tar")
    ):
        if is_tarfile(basename(path_file)):
            print("jest plik tar.gz")
            try:
                tar = tarfile.open(basename(path_file), "r:gz")
            except ReadError:
                print("reading problem")
                exit(0)
            metadata = tar.getnames()
            print(metadata)
            docker_manifests = "manifest.json"
            if docker_manifests in metadata:
                tar.extract("manifest.json", "")
                config, layers = check_json()
                counter = len(layers)
                counter_1 = 0
                print(config)
                if config[0] in metadata:
                    print("1 tag ok")
                for layer in layers:
                    if layer in metadata:
                        counter_1 += 1
                print(counter)
                if counter_1 == counter:
                    return path_file
    else:
        exit(1)


def check_images(file_images):
    print(file_images)
    images = list_images()
    return [file_image for file_image in file_images if file_image in images]


def list_images():
    client = from_env()
    try:
        return [
            image.attrs.setdefault("RepoTags", "")[0] for image in client.images.list()
        ]
    except APIError:
        print("error")


def create_file(tar_files, data, file_f, directory="/home/smartbss/"):
    with open(file_f, "w") as filename:
        for host in data:
            filename.write(f"# Implementation of the server environment: {host} \n")
            for tar_file in tar_files:
                filename.write(
                    f"scp -r {tar_file}  smartbss@{data[host]}:{directory}\n"
                )


def run_command(tar_files, host_ip, directory="/home/smartbss/", user="smartbss"):
    for keys in host_ip:
        print(keys, "->", host_ip[keys])
        ssh = createSSHClient(host_ip[keys], 22, user)
        scp = SCPClient(ssh.get_transport())
        for tar_file in tar_files:
            scp.put(tar_file, remote_path=directory)


def file_images(path_file):
    if (
        exists(path_file)
        and isfile(path_file)
        and str(basename(path_file)).endswith(".txt")
    ):
        with open(path_file, "r") as filename:
            file_images = set([file_image for file_image in filename.readlines()])
        images = check_images(file_images)
        return images
    else:
        print("Wprowadzony plik nie spełnia kryteri")
        exit(1)


base = basename(path)
def create_tar(images_tags):
    client = docker.from_env()
    tar_files_name = []
    for image_tags in images_tags:
        image = client.images.get(image_tags)
        if not exists(f"{image_tags}.tar"):
            with tarfile.open(f'{image_tags}', "w:gz") as tar:
            	for chunk in image.save():
            		tar.add(chunk)
            f = open(f"{image_tags}.tar", "wb")
            for chunk in image.save():
                f.write(chunk)
            f.close()
            tar_files_name.append(f"{image_tags}.tar")
    return tar_files_name

    # tar = tarfile.open(f"{image}.tar.gz", "w:gz")
    # tar.close()


if __name__ == "__main__":
    deployments = ("ehc", "valo", "cmp")
    envs = ("test", "dev", "preprod", "prod")
    servers = ("crm", "es", "valo")
    parser = ArgumentParser(
        description="Program do wysyłania obrazów docker na wdrożenia [cmp, valo, ehc]"
    )
    parser.add_argument(
        "-d", "--deployment", help="Nazwa wdrozenie [cmp, valo, ehc]", type=str
    )
    parser.add_argument(
        "-e",
        "--env",
        help="Nazwa środowisko [dev, test, preprod, stagging, prod]",
        type=str,
    )
    parser.add_argument("-s", "--server", help="Nazwa serwera [crm, sc, es]", type=str)
    parser.add_argument("-P", "--Path", help="Sciezka do spakowanego obrazu", type=str)
    parser.add_argument(
        "-Pi", "--PathImage", help="Scieżka do pliku txt z nazwami obrazow", type=str
    )
    parser.add_argument("-i", "--image", help="Nazwa obrazu", type=str)
    parser.add_argument("-f", "--file", help="Nazwa pliku", type=str)
    args = parser.parse_args()
    opt1_value = args.deployment
    opt2_value = args.env
    opt3_value = args.server
    opt4_value = args.Path
    opt5_value = args.PathImage
    opt6_value = args.image
    opt7_value = args.file

    if opt1_value in deployments and opt2_value in envs and opt3_value in servers:
        host_ip = find_deploy(opt1_value, opt2_value, opt3_value)
        if opt4_value:
            archiwum = file_exist(opt4_value)
            if archiwum:
                pass
                run_command(list(archiwum), host_ip)
        elif opt5_value:
            images = file_images(opt5_value)
            tar_files_name = create_tar(images)
            run_command(tar_files_name, host_ip)
        elif opt6_value:
            w = []
            w.append(opt6_value)
            image = check_images(w)
            if image:
                tar_files_name = create_tar(image)
                run_command(tar_files_name, host_ip)
                w = check_images(list(opt6_value))
        if opt7_value:
            print(host_ip)
            create_file(tar_files_name, host_ip, opt7_value)
        else:
            print("Wysyłane bezposrednio:  tymczasowo  funckjonalnosc wylaczona")
            run_command(tar_files_name, host_ip)

    elif opt1_value in deployments and opt2_value in envs and opt3_value is None:
        host_ip = find_deploy(opt1_value, opt2_value, "")
        if opt4_value:
            archiwum = file_exist(opt4_value)
            if archiwum:
                pass
                run_command(list(archiwum), host_ip)
        elif opt5_value:
            images = file_images(opt5_value)
            tar_files_name = create_tar(images)
            run_command(tar_files_name, host_ip)
        elif opt6_value:
            image = check_images(list(opt6_value))
            if image:
                tar_files_name = create_tar(image)
                print(tar_files_name)
            run_command(tar_files_name, host_ip)
            w = check_images(list(opt6_value))
        if opt7_value:
            print("funckcja -f ")
            create_file(tar_files_name, host_ip, opt7_value)
        else:
            print("Wysyłane bezposrednio:  tymczasowo  funckjonalnosc wylaczona")
            run_command(tar_files_name, host_ip)

    elif opt1_value in deployments and opt2_value is None and opt3_value is None:
        host_ip = find_deploy(opt1_value, "", "")
        if opt4_value:
            archiwum = file_exist(opt4_value)
            if archiwum:
                pass
                run_command(list(archiwum), host_ip)
        elif opt5_value:
            images = file_images(opt5_value)
            tar_files_name = create_tar(images)
            run_command(tar_files_name, host_ip)
        elif opt6_value:
            # w = check_images(list(opt6_value))
            image = check_images([str(opt6_value)])
            if image:
                tar_files_name = create_tar(image)
            run_command(tar_files_name, host_ip)
            w = check_images(list(opt6_value))
        if opt7_value:
            print("funckcja -f ")
            create_file(tar_files_name, host_ip, opt7_value)
        else:
            print("Wysyłane bezposrednio:  tymczasowo  funckjonalnosc wylaczona")
            run_command(tar_files_name, host_ip)
