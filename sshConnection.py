from paramiko import SSHClient, AutoAddPolicy


def createSSHClient(server, port, user):
    client = SSHClient()
    client.load_system_host_keys()
    client.set_missing_host_key_policy(AutoAddPolicy())
    client.connect(server, port, user)
    return client