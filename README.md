# Descriptions
skrypt do kopiowanie skompresowanych obrazow docker na serwery cmp, ehc, valo
## Prerequisites

* python3
* virtualenv package
* docker

## Setup

### Setup python dependencies

1. `cd ./scripts/Produktyzacja/copy_tar_files_on_servers`
2. (setup once) `virtualenv -p python3 copy`
3. `source copy/bin/activate`
4. (setup once)  `pip3 install -r requirements.txt`

## Cleanup

### Delete copy virtualenv

1. `rm -r copy`
